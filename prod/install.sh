cd /home/ubuntu/Unimony

echo '### clone plaanet-api'
git clone https://gitlab.com/unimony/uni-api.git
echo '### npm install uni-api'
cd uni-api
sudo npm install

echo '### clone plaanet-client'
cd /home/ubuntu/Unimony
git clone https://gitlab.com/unimony/uni-client.git
echo '### npm install uni-client'
cd uni-client
sudo npm install
sudo npm install -g serve
sudo npm run build


echo '### create services for production'
sudo cp /home/ubuntu/Unimony/uni-install/prod/uni-api.service /etc/systemd/system/uni-api.service
sudo cp /home/ubuntu/Unimony/uni-install/prod/uni-client.service /etc/systemd/system/uni-client.service

sudo chmod 755 /etc/systemd/system/uni-api.service
sudo chmod 755 /etc/systemd/system/uni-client.service

sudo systemctl enable uni-api
sudo systemctl enable uni-client

sudo systemctl start uni-api
sudo systemctl start uni-client
